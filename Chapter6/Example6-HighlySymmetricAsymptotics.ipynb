{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Highly Symmetric Walk Asymptotics\n",
    "We give a selection of asymptotics for highly symmetric walks.  **Note: In the textbook we derive explicit asymptotic formulas which can be computed by hand. Here we verify those formulas on the examples from the textbook by computing asymptotics directly using the smooth point asymptotic results from Chapter 5.**\n",
    "\n",
    "*Requirements: ore_algebra (partially)*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This cell imports the code for smooth asymptotics from Chapter 5\n",
    "\n",
    "# Set a parameter to help simplify some algebraic numbers\n",
    "maxima_calculus('algebraic: true;')\n",
    "\n",
    "# Procedure to get Hessian appearing in asymptotics\n",
    "# Input: H, member of the symbolic ring\n",
    "#        r, direction vector (which can contain symbolic entries)\n",
    "#        vars, vector of variables\n",
    "#        CP, a dictionary mapping elements of vars\n",
    "# Output: The Hessian H defined in Lemma 5.5 of the textbook at the point w defined by CP\n",
    "def getHes(H,r,vars,CP):\n",
    "    dd = len(vars)\n",
    "    V = zero_vector(SR,dd)\n",
    "    U = matrix(SR,dd)\n",
    "    M = matrix(SR,dd-1)\n",
    "\n",
    "    for j in range(dd):\n",
    "        V[j] = r[j]/r[-1]\n",
    "        for i in range(dd):\n",
    "            U[i,j] = vars[i]*vars[j]*diff(H,vars[i],vars[j])/vars[-1]/diff(H,vars[-1])\n",
    "    for i in range(dd-1):\n",
    "        for j in range(dd-1):\n",
    "            M[i,j] = V[i]*V[j] + U[i,j] - V[j]*U[i,-1] - V[i]*U[j,-1] + V[i]*V[j]*U[-1,-1]\n",
    "            if i == j: M[i,j] = M[i,j] + V[i]\n",
    "    return M(CP)\n",
    "\n",
    "# Procedure to apply differential operator to f and set all variables to zero\n",
    "# Input: dop, element of a DifferentialWeylAlgebra over a polynomial ring\n",
    "#        f, an element of the base polynomial ring of dop\n",
    "# Output: dop(f) evaluated when all variables are zero\n",
    "def eval_op(dop, f):\n",
    "    if len(f.parent().gens()) == 1:\n",
    "        return add([prod([factorial(k) for k in E[0][1]])*E[1]*f[E[0][1][0]] for E in dop])\n",
    "    else:\n",
    "        return add([prod([factorial(k) for k in E[0][1]])*E[1]*f[(v for v in E[0][1])] for E in dop])\n",
    "\n",
    "# Procedure to get critical points of rational function with denominator H, in direction r\n",
    "# Input: H, member of the symbolic ring\n",
    "#        r, direction vector (which can contain symbolic entries)\n",
    "#        vars, vector of variables\n",
    "# Output: Solutions (if found by solve) of the smooth critical point equations of H in the direction r\n",
    "def critpt(H,r,vars):\n",
    "    d = len(vars)\n",
    "    criteqs = [H] + [r[j]*vars[0]*diff(H,vars[0]) - r[0]*vars[j]*diff(H,vars[j]) for j in range(1,d)]\n",
    "    return solve(criteqs,vars,solution_dict=true)\n",
    "\n",
    "# Procedure to compute asymptotic contribution of a strictly minimal contributing point\n",
    "# Input: G, member of the symbolic ring\n",
    "#        H, member of the symbolic ring\n",
    "#        r, direction vector (which can contain symbolic entries)\n",
    "#        vars, vector of variables\n",
    "#        CP, a dictionary mapping elements of vars to coordinates of a strictly minimal contributing point\n",
    "#        M, positive integer describing the number of terms in the asymptotic expansion to compute\n",
    "#        g, parametrization of variable vars[-1] near CP, in terms of the remaining variables\n",
    "# Output: ex, pw, se such that ex*pw*(se+O(n^(M-1)) gives an asymptotic expansion of the r-diagonal of \n",
    "#         G/H in the variables vars, to order M.\n",
    "# NOTE: Unlike the textbook, M here refers to the number of terms in the expansion\n",
    "#       (not the order of the expansion, so M should be at least 1)\n",
    "def smoothContrib(G,H,r,vars,CP,M,g):\n",
    "    # Preliminary definitions\n",
    "    dd = len(vars)\n",
    "    field = SR\n",
    "    tvars = list(var('t%d'%i) for i in range(dd-1))\n",
    "    dvars = list(var('dt%d'%i) for i in range(dd-1))\n",
    "\n",
    "    # Define differential Weyl algebra and set variable names\n",
    "    W = DifferentialWeylAlgebra(PolynomialRing(field,tvars))\n",
    "    WR = W.base_ring()\n",
    "    T = PolynomialRing(field,tvars).gens()\n",
    "    D = list(W.differentials())\n",
    "\n",
    "    # Compute Hessian matrix and differential operator Epsilon\n",
    "    HES = getHes(H,r,vars,CP)\n",
    "    HESinv = HES.inverse()\n",
    "    v = matrix(W,[D[k] for k in range(dd-1)])\n",
    "    Epsilon = -(v * HESinv.change_ring(W) * v.transpose())[0,0]\n",
    "\n",
    "    # Define quantities for calculating asymptotics\n",
    "    tsubs = [v == v.subs(CP)*exp(I*t) for [v,t] in zip(vars,tvars)]\n",
    "    tsubs += [vars[-1]==g.subs(tsubs)]\n",
    "    P = (-G/g/diff(H,vars[-1])).subs(tsubs)\n",
    "    psi = log(g.subs(tsubs)/g.subs(CP)) + I * add([r[k]*tvars[k] for k in range(dd-1)])/r[-1]\n",
    "    v = matrix(SR,[tvars[k] for k in range(dd-1)])\n",
    "    psiTilde = psi - (v * HES * v.transpose())[0,0]/2\n",
    "\n",
    "    # Recursive function to convert symbolic expression to polynomial in t variables\n",
    "    def to_poly(p,k):\n",
    "        if k == 0:\n",
    "            return add([a*T[k]^int(b) for [a,b] in p.coefficients(tvars[k])])\n",
    "        return add([to_poly(a,k-1)*T[k]^int(b) for [a,b] in p.coefficients(tvars[k])])\n",
    "\n",
    "    # Compute Taylor expansions to sufficient orders\n",
    "    N = 2*M\n",
    "    PsiSeries = to_poly(taylor(psiTilde,*((v,0) for v in tvars), N),dd-2)\n",
    "    PSeries = to_poly(taylor(P,*((v,0) for v in tvars), N),dd-2)\n",
    "\n",
    "    # Precompute products used for asymptotics\n",
    "    EE = [Epsilon^k for k in range(3*M-2)]\n",
    "    PP = [PSeries] + [0 for k in range(2*M-2)]\n",
    "    for k in range(1,2*M-1):\n",
    "        PP[k] = PP[k-1]*PsiSeries\n",
    "    \n",
    "    # Function to compute constants appearing in asymptotic expansion\n",
    "    def Clj(l,j):\n",
    "        return (-1)^j*SR(eval_op(EE[l+j],PP[l]))/(2^(l+j)*factorial(l)*factorial(l+j))\n",
    "    \n",
    "    # Compute different parts of asymptotic expansion\n",
    "    var('n')\n",
    "    ex = (prod([1/v^k for (v,k) in zip(vars,r)]).subs(CP).canonicalize_radical())^n\n",
    "    pw = (r[-1]*n)^((1-dd)/2)\n",
    "    se = sqrt((2*pi)^(1-dd)/HES.det()) * add([add([Clj(l,j) for l in range(2*j+1)])/(r[-1]*n)^j for j in range(M)])\n",
    "    \n",
    "    return ex, pw, se.canonicalize_radical()\n",
    "\n",
    "# Procedure to aid in printing an asymptotic expansion\n",
    "# Procedure to get critical points of rational function with denominator H, in direction r\n",
    "# Input: ex,pw,se as returned by smoothContrib(G,H,r,vars,CP,M,g)\n",
    "# Output: None (function pretty prints the asymptotic expression defined by ex,pw,se, and M)\n",
    "def disp_asm(ex,pw,se,M):\n",
    "    show(ex*pw,LatexExpr(\"\\\\Bigg(\"), se, LatexExpr(\"+ O\\\\Bigg(\"), n^(-M), LatexExpr(\"\\\\Bigg)\\\\Bigg)\"))\n",
    "\n",
    "# Test if all coordinates in a list of numbers are real and positive\n",
    "def pos_coords(L):\n",
    "    return all(map(lambda c: (c>0) and c.is_real(), L))\n",
    "\n",
    "# Find critical points with real positive coordinates\n",
    "def walk_pos_critpt(L,vars):\n",
    "    return filter(lambda l: pos_coords([v.subs(l) for v in vars]), L)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example 6.2 (Highly Symmetric Models in Two Dimensions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The number of quadrant models on the highly-symmetric step set [(0, 1), (0, -1), (1, 0), (-1, 0)] has asymptotic expansion\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{4^{n}}{n} \\Bigg( \\frac{4}{\\pi} + O\\Bigg( \\frac{1}{n} \\Bigg)\\Bigg)</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{4^{n}}{n} \\Bigg( \\frac{4}{\\pi} + O\\Bigg( \\frac{1}{n} \\Bigg)\\Bigg)\n",
       "\\end{math}"
      ],
      "text/plain": [
       "4^n/n \\Bigg( 4/pi + O\\Bigg( 1/n \\Bigg)\\Bigg)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The number of quadrant models on the highly-symmetric step set [(1, 1), (1, -1), (-1, 1), (-1, -1)] has asymptotic expansion\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{4^{n}}{n} \\Bigg( \\frac{2}{\\pi} + O\\Bigg( \\frac{1}{n} \\Bigg)\\Bigg)</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{4^{n}}{n} \\Bigg( \\frac{2}{\\pi} + O\\Bigg( \\frac{1}{n} \\Bigg)\\Bigg)\n",
       "\\end{math}"
      ],
      "text/plain": [
       "4^n/n \\Bigg( 2/pi + O\\Bigg( 1/n \\Bigg)\\Bigg)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The number of quadrant models on the highly-symmetric step set [(0, 1), (0, -1), (1, 1), (1, -1), (-1, 1), (-1, -1)] has asymptotic expansion\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{6^{n}}{n} \\Bigg( \\frac{\\sqrt{3} \\sqrt{2}}{\\pi} + O\\Bigg( \\frac{1}{n} \\Bigg)\\Bigg)</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{6^{n}}{n} \\Bigg( \\frac{\\sqrt{3} \\sqrt{2}}{\\pi} + O\\Bigg( \\frac{1}{n} \\Bigg)\\Bigg)\n",
       "\\end{math}"
      ],
      "text/plain": [
       "6^n/n \\Bigg( sqrt(3)*sqrt(2)/pi + O\\Bigg( 1/n \\Bigg)\\Bigg)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The number of quadrant models on the highly-symmetric step set [(0, 1), (0, -1), (1, 0), (-1, 0), (-1, 1), (-1, -1), (1, -1), (1, 1)] has asymptotic expansion\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{8^{n}}{n} \\Bigg( \\frac{8}{3 \\, \\pi} + O\\Bigg( \\frac{1}{n} \\Bigg)\\Bigg)</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{8^{n}}{n} \\Bigg( \\frac{8}{3 \\, \\pi} + O\\Bigg( \\frac{1}{n} \\Bigg)\\Bigg)\n",
       "\\end{math}"
      ],
      "text/plain": [
       "8^n/n \\Bigg( 8/3/pi + O\\Bigg( 1/n \\Bigg)\\Bigg)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Enter the short step sets defining highly symmetric quadrant models\n",
    "N = (0,1); SS = (0,-1); E = (1,0); W = (-1,0);\n",
    "NE = (1,1); NW = (-1,1); SE = (1,-1); SW = (-1,-1);\n",
    "HS1  = [N,SS,E,W]\n",
    "HS2  = [NE,SE,NW,SW]\n",
    "HS3  = [N,SS,NE,SE,NW,SW]\n",
    "HS4  = [N,SS,E,W,NW,SW,SE,NE]\n",
    "\n",
    "# Define basic quantities\n",
    "var('x,y,t')\n",
    "vars = [x,y,t]\n",
    "r = [1,1,1]\n",
    "M = 1\n",
    "\n",
    "# Loop through the four models under consideration\n",
    "for ST in [HS1,HS2,HS3,HS4]:\n",
    "    # Define the rational function for the model\n",
    "    S = add([x^i*y^j for (i,j) in ST])\n",
    "    G = (1+x)*(1+y)\n",
    "    H = 1-t*x*y*S\n",
    "\n",
    "    # Get the minimal critical point with positive coordinates (and prove it is unique)\n",
    "    [cp] = walk_pos_critpt(critpt(H,r,vars),vars)\n",
    "    \n",
    "    # Compute and print asymptotics\n",
    "    g = solve(H,vars[-1])[0].rhs()\n",
    "    ex,pw,se = smoothContrib(G,H,r,vars,cp,M,g)\n",
    "    print(\"The number of quadrant models on the highly-symmetric step set {} has asymptotic expansion\".format(ST))\n",
    "    disp_asm(ex,pw,se,M)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example 6.3 (A Weighted Model)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The number of weighted quadrant models has asymptotic expansion\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{{\\left(4 \\, a + 2 \\, b + 2 \\, c\\right)}^{n}}{n} \\Bigg( \\frac{2 \\, {\\left(2 \\, a + b + c\\right)}}{\\pi \\sqrt{2 \\, a + b} \\sqrt{2 \\, a + c}} + O\\Bigg( \\frac{1}{n} \\Bigg)\\Bigg)</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{{\\left(4 \\, a + 2 \\, b + 2 \\, c\\right)}^{n}}{n} \\Bigg( \\frac{2 \\, {\\left(2 \\, a + b + c\\right)}}{\\pi \\sqrt{2 \\, a + b} \\sqrt{2 \\, a + c}} + O\\Bigg( \\frac{1}{n} \\Bigg)\\Bigg)\n",
       "\\end{math}"
      ],
      "text/plain": [
       "(4*a + 2*b + 2*c)^n/n \\Bigg( 2*(2*a + b + c)/(pi*sqrt(2*a + b)*sqrt(2*a + c)) + O\\Bigg( 1/n \\Bigg)\\Bigg)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Define basic quantities\n",
    "var('x,y,t,a,b,c')\n",
    "assume(a>0,b>0,c>0)\n",
    "vars = [x,y,t]\n",
    "r = [1,1,1]\n",
    "\n",
    "# Define the rational function for the model\n",
    "S = (y+1/y)*(a*x+b+a/x) + (c*x+c/x)\n",
    "G = (1+x)*(1+y)\n",
    "H = 1-t*x*y*S\n",
    "\n",
    "# Get the minimal critical point with positive coordinates (and prove it is unique)\n",
    "[cp] = list(walk_pos_critpt(critpt(H,r,vars),vars))\n",
    "cp\n",
    "\n",
    "# Compute and print asymptotics\n",
    "M = 1\n",
    "g = solve(H,vars[-1])[0].rhs()\n",
    "ex,pw,se = smoothContrib(G,H,r,vars,cp,M,g)\n",
    "print(\"The number of weighted quadrant models has asymptotic expansion\")\n",
    "disp_asm(ex,pw,se.factor(),M)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example 6.5 (Higher Asymptotics Terms of Simple Walks)\n",
    "Note: this matches the computations for Example 5.6 in Chapter 5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The asymptotic contribution of {x: 1, y: 1, t: 1/4} to diagonal asymptotics is\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{4^{n}}{n} \\Bigg( \\frac{4}{\\pi} - \\frac{6}{\\pi n} + \\frac{19}{2 \\, \\pi n^{2}} - \\frac{63}{4 \\, \\pi n^{3}} + O\\Bigg( \\frac{1}{n^{4}} \\Bigg)\\Bigg)</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{4^{n}}{n} \\Bigg( \\frac{4}{\\pi} - \\frac{6}{\\pi n} + \\frac{19}{2 \\, \\pi n^{2}} - \\frac{63}{4 \\, \\pi n^{3}} + O\\Bigg( \\frac{1}{n^{4}} \\Bigg)\\Bigg)\n",
       "\\end{math}"
      ],
      "text/plain": [
       "4^n/n \\Bigg( 4/pi - 6/(pi*n) + 19/2/(pi*n^2) - 63/4/(pi*n^3) + O\\Bigg( n^(-4) \\Bigg)\\Bigg)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The asymptotic contribution of {x: -1, y: -1, t: -1/4} to diagonal asymptotics is\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{\\left(-4\\right)^{n}}{n} \\Bigg( \\frac{1}{\\pi n^{2}} - \\frac{9}{2 \\, \\pi n^{3}} + O\\Bigg( \\frac{1}{n^{4}} \\Bigg)\\Bigg)</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{\\left(-4\\right)^{n}}{n} \\Bigg( \\frac{1}{\\pi n^{2}} - \\frac{9}{2 \\, \\pi n^{3}} + O\\Bigg( \\frac{1}{n^{4}} \\Bigg)\\Bigg)\n",
       "\\end{math}"
      ],
      "text/plain": [
       "(-4)^n/n \\Bigg( 1/(pi*n^2) - 9/2/(pi*n^3) + O\\Bigg( n^(-4) \\Bigg)\\Bigg)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Define basic quantities\n",
    "var('x,y,t')\n",
    "r = [1,1,1]\n",
    "vars = [x,y,t]\n",
    "\n",
    "# Define the rational function for the model\n",
    "S = add([x^i*y^j for (i,j) in HS1])\n",
    "G = (1+x)*(1+y)\n",
    "H = 1-t*x*y*S\n",
    "\n",
    "# Find both (minimal) critical points\n",
    "[sigma, tau] = critpt(H,r,vars)\n",
    "\n",
    "# Determine the asymptotic contribution of each critical point\n",
    "M = 4\n",
    "g = solve(H,vars[-1])[0].rhs()\n",
    "ex1,pw1,se1 = smoothContrib(G,H,r,vars,sigma,M,g)\n",
    "ex2,pw2,se2 = smoothContrib(G,H,r,vars,tau,M,g)\n",
    "\n",
    "# Print the asymptotic contribution of each critical point\n",
    "print(\"The asymptotic contribution of {} to diagonal asymptotics is\".format(sigma))\n",
    "disp_asm(ex1,pw1,se1.expand(),M)\n",
    "print(\"The asymptotic contribution of {} to diagonal asymptotics is\".format(tau))\n",
    "disp_asm(ex2,pw2,se2.expand(),M)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\left[(4)^{n}n^{-1}\\Bigl(1 - \\frac{3}{2}n^{-1} + \\frac{19}{8}n^{-2} - \\frac{63}{16}n^{-3} + \\frac{871}{128}n^{-4} + O(n^{-5})\\Bigr), (-4)^{n}n^{-3}\\Bigl(1 - \\frac{9}{2}n^{-1} + \\frac{107}{8}n^{-2} - \\frac{525}{16}n^{-3} + \\frac{9263}{128}n^{-4} + O(n^{-5})\\Bigr)\\right]</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\left[(4)^{n}n^{-1}\\Bigl(1 - \\frac{3}{2}n^{-1} + \\frac{19}{8}n^{-2} - \\frac{63}{16}n^{-3} + \\frac{871}{128}n^{-4} + O(n^{-5})\\Bigr), (-4)^{n}n^{-3}\\Bigl(1 - \\frac{9}{2}n^{-1} + \\frac{107}{8}n^{-2} - \\frac{525}{16}n^{-3} + \\frac{9263}{128}n^{-4} + O(n^{-5})\\Bigr)\\right]\n",
       "\\end{math}"
      ],
      "text/plain": [
       "[4^n*n^(-1)*(1 - 3/2*n^(-1) + 19/8*n^(-2) - 63/16*n^(-3) + 871/128*n^(-4) + O(n^(-5))),\n",
       " (-4)^n*n^(-3)*(1 - 9/2*n^(-1) + 107/8*n^(-2) - 525/16*n^(-3) + 9263/128*n^(-4) + O(n^(-5)))]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Note that additional terms in the expansion can be computed very efficiently \n",
    "# from a P-recurrence satisfied by the diagonal sequence / D-finite equation of the GF\n",
    "from ore_algebra import *\n",
    "Pols.<n> = PolynomialRing(QQ)\n",
    "Shift.<Sn> = OreAlgebra(Pols)\n",
    "\n",
    "# Import recurrence satisfied by diagonal sequence (obtained from kernel method and creative telescoping)\n",
    "rec = (-n^2 - 7*n - 12)*Sn^2 + (8*n + 20)*Sn + 16*n^2 + 48*n + 32\n",
    "\n",
    "# The asymptotic expansion of the diagonal will be a C-linear combination of these terms\n",
    "# Looking at the dominant terms allows one to compute the coefficients (here 4/pi and 1/pi)\n",
    "# This approach can compute about 20 terms in the expansion in one second on a modern laptop\n",
    "show(rec.generalized_series_solutions(5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example 6.9 (Higher Order Constants for Simple Walk in Two Dimensions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set basic info\n",
    "var('x,y,t,a,b')\n",
    "assume(a,'integer')\n",
    "assume(b,'integer')\n",
    "r = [1,1,1]\n",
    "vars = [x,y,t]\n",
    "\n",
    "# Define the rational function for the number of quadrant walks on [N,S,E,W]\n",
    "# which start at the parametrized point (a,b)\n",
    "S = add([x^i*y^j for (i,j) in HS1])\n",
    "G = (1-x^(2*a+2))*(1-y^(2*b+2))/x^a/y^b/(1-x)/(1-y)\n",
    "H = 1-t*x*y*S\n",
    "\n",
    "# Find both (minimal) critical points\n",
    "[sigma, tau] = critpt(H,r,vars)\n",
    "\n",
    "# Determine the asymptotic contribution of each critical point\n",
    "# (this can take a few minutes to compute the higher order terms)\n",
    "M = 3\n",
    "g = solve(H,vars[-1])[0].rhs()\n",
    "ex1,pw1,se1 = smoothContrib(G,H,r,vars,sigma,M,g)\n",
    "ex2,pw2,se2 = smoothContrib(G,H,r,vars,tau,M,g)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The asymptotic contribution of {x: 1, y: 1, t: 1/4} to diagonal asymptotics is\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{4^{n}}{n} \\Bigg( \\frac{4 \\, {\\left(a + 1\\right)} {\\left(b + 1\\right)}}{\\pi} - \\frac{2 \\, {\\left(2 \\, a^{2} + 2 \\, b^{2} + 4 \\, a + 4 \\, b + 9\\right)} {\\left(a + 1\\right)} {\\left(b + 1\\right)}}{3 \\, \\pi n} + \\frac{{\\left(36 \\, a^{4} + 40 \\, a^{2} b^{2} + 36 \\, b^{4} + 144 \\, a^{3} + 80 \\, a^{2} b + 80 \\, a b^{2} + 144 \\, b^{3} + 516 \\, a^{2} + 160 \\, a b + 516 \\, b^{2} + 744 \\, a + 744 \\, b + 855\\right)} {\\left(a + 1\\right)} {\\left(b + 1\\right)}}{90 \\, \\pi n^{2}} + O\\Bigg( \\frac{1}{n^{3}} \\Bigg)\\Bigg)</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{4^{n}}{n} \\Bigg( \\frac{4 \\, {\\left(a + 1\\right)} {\\left(b + 1\\right)}}{\\pi} - \\frac{2 \\, {\\left(2 \\, a^{2} + 2 \\, b^{2} + 4 \\, a + 4 \\, b + 9\\right)} {\\left(a + 1\\right)} {\\left(b + 1\\right)}}{3 \\, \\pi n} + \\frac{{\\left(36 \\, a^{4} + 40 \\, a^{2} b^{2} + 36 \\, b^{4} + 144 \\, a^{3} + 80 \\, a^{2} b + 80 \\, a b^{2} + 144 \\, b^{3} + 516 \\, a^{2} + 160 \\, a b + 516 \\, b^{2} + 744 \\, a + 744 \\, b + 855\\right)} {\\left(a + 1\\right)} {\\left(b + 1\\right)}}{90 \\, \\pi n^{2}} + O\\Bigg( \\frac{1}{n^{3}} \\Bigg)\\Bigg)\n",
       "\\end{math}"
      ],
      "text/plain": [
       "4^n/n \\Bigg( 4*(a + 1)*(b + 1)/pi - 2/3*(2*a^2 + 2*b^2 + 4*a + 4*b + 9)*(a + 1)*(b + 1)/(pi*n) + 1/90*(36*a^4 + 40*a^2*b^2 + 36*b^4 + 144*a^3 + 80*a^2*b + 80*a*b^2 + 144*b^3 + 516*a^2 + 160*a*b + 516*b^2 + 744*a + 744*b + 855)*(a + 1)*(b + 1)/(pi*n^2) + O\\Bigg( n^(-3) \\Bigg)\\Bigg)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The asymptotic contribution of {x: -1, y: -1, t: -1/4} to diagonal asymptotics is\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{\\left(-4\\right)^{n}}{n} \\Bigg( \\frac{{\\left(\\left(-1\\right)^{a} {\\left(a + 1\\right)} b + \\left(-1\\right)^{a} {\\left(a + 1\\right)}\\right)} \\left(-1\\right)^{b}}{\\pi n^{2}} + O\\Bigg( \\frac{1}{n^{3}} \\Bigg)\\Bigg)</script></html>"
      ],
      "text/latex": [
       "\\begin{math}\n",
       "\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{\\left(-4\\right)^{n}}{n} \\Bigg( \\frac{{\\left(\\left(-1\\right)^{a} {\\left(a + 1\\right)} b + \\left(-1\\right)^{a} {\\left(a + 1\\right)}\\right)} \\left(-1\\right)^{b}}{\\pi n^{2}} + O\\Bigg( \\frac{1}{n^{3}} \\Bigg)\\Bigg)\n",
       "\\end{math}"
      ],
      "text/plain": [
       "(-4)^n/n \\Bigg( ((-1)^a*(a + 1)*b + (-1)^a*(a + 1))*(-1)^b/(pi*n^2) + O\\Bigg( n^(-3) \\Bigg)\\Bigg)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Asymptotics for the model with parametrized start is \n",
    "# given by adding these two asymptotic contributions\n",
    "\n",
    "print(\"The asymptotic contribution of {} to diagonal asymptotics is\".format(sigma))\n",
    "var('N')\n",
    "se_simp1 = add([k.factor()/n^p for [k,p] in se1.subs(n=1/N).series(N,M).coefficients()])\n",
    "disp_asm(ex1,pw1,se_simp1,M)\n",
    "\n",
    "print(\"The asymptotic contribution of {} to diagonal asymptotics is\".format(tau))\n",
    "disp_asm(ex2,pw2,se2,M)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SageMath 9.2",
   "language": "sage",
   "name": "sagemath"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
