# An Invitation to Analytic Combinatorics

This repository hosts the Sage and Maple worksheets for the textbook An Invitation to Analytic Combinatorics: From One to Several Variables, by Stephen Melczer. Further details, and the book manuscript, can be found on https://melczer.ca/textbook/